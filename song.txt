In the moonlight, whispers softly play,
Stars above, painting dreams in the night's ballet.
Lost in echoes of a love untold,
Chasing melodies, like secrets unfold.

Oh, dance with me in this sweet reverie,
Notes like butterflies, set our spirits free.
In the rhythm of the heart's gentle sighs,
We're writing lyrics under starlit skies.
